# External Settings for Django projects

This library provides a mechanism for Django projects to load their settings
from external YAML-formatted documents. External sources include:

* YAML values in environment variables with a configurable prefix.
* YAML documents available over HTTPS.
* YAML documents on the local filesystem.
* YAML documents in a Google Cloud Storage object.
* YAML documents in a Google Secret Manager secret.

Any URL supported by the [geddit library](https://pypi.org/project/geddit/) may
be used.

## Quick start

Install directly from the GitLab package registry:

```bash
pip install externalsettings --index-url https://gitlab.developers.cam.ac.uk/api/v4/projects/2026/packages/pypi/simple
```

The module does *not* yet need adding to `INSTALLED_APPS` but it is safe to do
so if you wish.

The main interface to the module is `externalsettings.load_external_settings`.
Generally you use this function at the top of your `settings.py` file. For
example, you can have your project load extra settings from a comma-separated
list of URLs passed in the `EXTRA_SETTINGS_URLS` environment variable:

```python
# settings.py
import externalsettings

# ...

# If the EXTRA_SETTINGS_URLS environment variable is set, it is a
# comma-separated list of URLs from which to fetch additional settings as
# YAML-formatted documents. The documents should be dictionaries and top-level
# keys are imported into this module's global values.
_external_setting_urls = []
_external_setting_urls_list = os.environ.get('EXTRA_SETTINGS_URLS', '').strip()
if _external_setting_urls_list != '':
    _external_setting_urls.extend(_external_setting_urls_list.split(','))

externalsettings.load_external_settings(
    globals(), urls=_external_setting_urls,
    required_settings=[
        'SECRET_KEY', 'DATABASES',
    ],
    optional_settings=[
        'EMAIL_HOST', 'EMAIL_HOST_PASSWORD', 'EMAIL_HOST_USER', 'EMAIL_PORT',
    ],
)
```

In this example, settings can be loaded from external documents listed in
`EXTRA_SETTINGS_URLS` or from the environment variables of the form
`EXTERNAL_SETTING_[setting name]`. Environment variable values are interpreted
as YAML values so, for example setting `EXTERNAL_SETTING_EMAIL_PORT` to the
string "1025" would cause the `EMAIL_PORT` setting to have the *numeric* value
of 1025.

Setting which *must* in one way or another come from external sources are listed
in `required_settings`. Settings which *may* appear in external sources are
listed in `optional_settings`.

If a setting appears in an external source which is not listed in either
`required_settings` or `optional_settings`, an
`externalsettings.InvalidExternalSettingsError` exception is raised. This
behaviour can be suppressed by setting the `allow_unrecognised_settings` flag
to True. In addition, if `allow_unrecognised_settings` is set to True,
additional settings found will be removed from the settings by default, but can
be left in-place by setting `strip_unrecognised_settings` to False.


If no value is provided for a setting listed in `required_settings`, an
`externalsettings.InvalidExternalSettingsError` exception is raised.

Settings are loaded in the order passed to `urls` and then from environment
variables.

If a loaded setting is a dictionary value and there is an existing setting with
that value, the dictionary is "deep-merged" into the value. If the existing
setting is not a dictionary then the external setting replaces it.

## Testing

This project is based on a cut-down version of the UIS DevOps Division's
standard [Django
boilerplate](https://gitlab.developers.cam.ac.uk/uis/devops/webapp-boilerplate/)
with non-testing functionality removed. To run the test suite:

1. [Install docker-compose](https://docs.docker.com/compose/install/).
2. Run the `./tox.sh` script.

## CI configuration

The project is configured with Gitlab AutoDevOps via Gitlab CI using the
.gitlab-ci.yml file.

## Copyright License

See the [LICENSE](LICENSE) file for details.
