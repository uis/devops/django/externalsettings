# Requirements for the "stub" project used for testing the application. The
# exact version of Django isn't specified since the testing environment may have
# installed a specific one.
django

# For connecting to the database.
psycopg2-binary

# For loading fixtures.
PyYAML

# For an improved manage.py shell experience.
ipython

# So that tests may be run within the container.
tox
