# Use python alpine image to run webapp proper
FROM registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/python:3.10-alpine

# Ensure packages are up to date and install some useful utilities
RUN apk update && apk add git vim postgresql-dev libffi-dev gcc musl-dev \
	libxml2-dev libxslt-dev

# From now on, work in the application directory
WORKDIR /usr/src/app

# Copy Docker configuration and install any requirements. We install
# requirements/docker.txt last to allow it to override any versions in
# requirements/requirements.txt.
ADD ./requirements.txt ./
ADD ./requirements/* ./requirements/
RUN pip install --no-cache-dir -r requirements.txt && \
	pip install --no-cache-dir -r requirements/base.txt && \
	pip install --no-cache-dir -r requirements/docker.txt

# This image is *only* used to run tests so needs no entrypoint or collectstatic
# running but does need all the files added.
ADD ./ ./
