"""
Django External Settings URL Configuration

"""
from django.contrib import admin
from django.http import HttpResponse
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('healthz', lambda request: HttpResponse('ok', content_type="text/plain"), name='healthz'),
]
