from setuptools import find_packages, setup

setup(
    name="externalsettings",
    version="1.1.0",
    packages=find_packages(),
    install_requires=[
        "PyYAML",
        "geddit~=1.0"
    ],
)
