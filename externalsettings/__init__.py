"""
Support for loading Django settings from external sources.

"""
from .loader import (  # noqa: F401
    DEFAULT_ENV_PREFIX, load_external_settings, InvalidExternalSettingsError
)
