import os
import logging

import geddit
import yaml

LOG = logging.getLogger()

DEFAULT_ENV_PREFIX = 'EXTERNAL_SETTING_'


class InvalidExternalSettingsError(RuntimeError):
    """
    Exception raised when external settings were somehow invalid. Usually this is because a
    required setting was not set or an setting did not appear in the list of required or optional
    settings.

    """


def load_external_settings(
        globals_, urls=[], env_prefix=DEFAULT_ENV_PREFIX, required_settings=[],
        optional_settings=[], allow_unrecognised_settings=False, strip_unrecognised_settings=True):
    """
    Load settings from external sources and merge into an existing globals dict. It is envisaged
    that one would call this from settings.py in the following manner:

        # settings.py

        from externalsettings import load_external_settings

        load_external_settings(
            globals(),
            # ... other args ...
        )

    External settings are loaded from YAML-formatted documents. The document should be a mapping
    which maps setting names to values.

    Settings are initially loaded from a list of URLs specified by the *urls* argument in order.
    URLs can have the following schemes:

    * "file" (or no scheme) loads a YAML document from a local file.
    * "https" loads a YAML document over HTTP using TLS.
    * "gs" loads a YAML document stored as an object in a Google Storage bucket. The format of the
        URL matches gsutil and is of the form "gs://[BUCKET]/[OBJECT PATH]".
    * "sm" loads a secret from a Google Secret Manager secret. The format is taken from the Berglas
        project[1] and is "sm://[PROJECT]/[SECRET]#[VERSION]". The version fragement is optional.
        If unspecified, the latest secret is used.

    [1] https://github.com/GoogleCloudPlatform/berglas/blob/master/doc/reference-syntax.md

    After loading settings from URLs, settings are then loaded as YAML-formatted values from the
    local environment. Environment variables must be prefixed. The default prefix is
    "EXTERNAL_SETTING_" but can be overridden via the *env_prefix* argument.

    All settings, whether loaded from URLs or the environment, must appear in one of the
    *required_settings* or *optional_settings*. If a setting is loaded without being mentioned in
    one of them, an ``InvalidExternalSettingsError`` exception is raised. This behaviour can be
    suppressed by setting the ``allow_unrecognised_settings`` flag to True. In addition, if
    ``allow_unrecognised_settings`` is set to True, additional settings found will be removed from
    the settings by default, but can be left in-place by setting ``strip_unrecognised_settings`` to
    False.

    If a settings appears in ``required_settings`` but is not loaded from any external source,
    ``InvalidExternalSettingsError`` is raised.

    If setting values are dicts, they are deep-merged with any previous values. This can be used to
    provide non-secret or default values in a YAML file on the local filesystem and "poke" secret
    or custom values into them.

    As an example, consider the following three resources:

        # https://example.com/base-settings.yaml
        DATABASES:
            default:
                ENGINE: django.db.backends.postgresql
                HOST: db-host
                NAME: db
                USER: dbuser
                CONN_MAX_AGE: 60 # seconds

        # sm://my-project/secret-settings
        SECRET_KEY: some-secret-key

        DATABASES:
            default:
                PASSWORD: super-secret

        # settings.py
        from externalsettings import load_external_settings
        load_external_settings(
            globals(),
            urls=[
                'https://example.com/base-settings.yaml',
                'sm://my-project/secret-settings',
            ],
            required_settings=['DATABASES', 'SECRET_KEY']
        )

    In this example, non-secret configuration for the database is held on a publicly accessible
    HTTPS server and secret information is stored in a Google Secret Manager secret. The secret
    information is merged into the settings. If the secret key is not set or no database
    information is provided, an system check error is raised.

    """
    _dict_deep_update(globals_, _fetch_external_settings(
        urls=urls, env_prefix=env_prefix, required_settings=required_settings,
        optional_settings=optional_settings,
        allow_unrecognised_settings=allow_unrecognised_settings,
        strip_unrecognised_settings=strip_unrecognised_settings
    ))


def _fetch_external_settings(
        *, urls, env_prefix, required_settings, optional_settings, allow_unrecognised_settings,
        strip_unrecognised_settings):
    """
    Load any external settings into a dictionary keyed by setting name.

    """
    loaded_settings = {}

    # Update settings from external URLs.
    for url in urls:
        LOG.info('Loading settings from %s', url)
        _dict_deep_update(loaded_settings, _import_settings_url(url))

    # Determine which settings are allowed to be external
    allowed_settings = set(required_settings) | set(optional_settings)

    # Update settings from environment
    _dict_deep_update(loaded_settings, {
        var[len(env_prefix):]: yaml.safe_load(os.environ[var])
        for var in (
            k for k in os.environ.keys() if k.startswith(env_prefix)
        )
    })

    # Form a set of settings which were loaded without being allowed.
    disallowed_settings = set(loaded_settings.keys()) - allowed_settings
    if allow_unrecognised_settings:
        if strip_unrecognised_settings:
            for key in disallowed_settings:
                del loaded_settings[key]
    elif len(disallowed_settings) > 0:
        raise InvalidExternalSettingsError(
            'Unexpected external settings: %r' %
            disallowed_settings
        )

    # Form a set of settings which were not loaded but were expected
    omitted_settings = set(required_settings) - set(loaded_settings.keys())
    if len(omitted_settings) > 0:
        raise InvalidExternalSettingsError(
            'Expected external settings not set: %r' %
            omitted_settings
        )

    return loaded_settings


def _dict_deep_update(d1, d2):
    """
    "Deep" version of d1.update(d2). If d1[k] and d2[k] exists and are both dicts then d1[k]
    is updated with d2[k] and so on recursively.

    Returns d1 for convenience.

    """
    d1.update({
        k: (
            _dict_deep_update(d1[k], v)
            if isinstance(d1.get(k), dict) and isinstance(v, dict)
            else v
        )
        for k, v in d2.items()
    })
    return d1


def _import_settings_url(url):
    """
    Load settings as a YAML document.

    """
    LOG.info('Importing settings from %s', url)
    return yaml.safe_load(geddit.geddit(url))
