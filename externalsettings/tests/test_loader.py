import unittest.mock

from django.conf import settings
from django.test import TestCase, override_settings
import yaml

from .. import DEFAULT_ENV_PREFIX, load_external_settings, InvalidExternalSettingsError


# override_settings() here is used only to make sure we have a *copy* of the settings object within
# tests so that we don't actually mess up real settings when we call load_external_settings().
@override_settings()
class Loader(TestCase):
    def test_environment_setting(self):
        """
        Settings are loaded from appropriately prefixed environment variables.

        """
        env = {
            f'{DEFAULT_ENV_PREFIX}FOO': 'bar',
            f'XXX_{DEFAULT_ENV_PREFIX}BAR': 'buzz',
        }
        with self.environment(**env):
            load_external_settings(settings.__dict__, optional_settings=['FOO', 'BAR'])
            self.assertEqual(settings.FOO, 'bar')

            # Only FOO was loaded from environment
            self.assertFalse(hasattr(settings, 'BAR'))

    def test_url_setting(self):
        """
        Settings are loaded from external settings.

        """
        with self.url_map({'https://example.invalid/s1': {'FOO': 'bar'}}):
            load_external_settings(settings.__dict__, optional_settings=['FOO'])
            self.assertEqual(settings.FOO, 'bar')

    def test_url_setting_order(self):
        """
        Settings are loaded from external settings and the last one "wins".

        """
        url_map = {
            'https://example.invalid/s1': {'FOO': 'bar', 'BING': 'boo'},
            'https://example.invalid/s2': {'FOO': 'buzz'},
        }
        with self.url_map(url_map):
            load_external_settings(settings.__dict__, urls=[
                'https://example.invalid/s1', 'https://example.invalid/s2',
            ], optional_settings=['FOO', 'BING'])
            self.assertEqual(settings.FOO, 'buzz')
            self.assertEqual(settings.BING, 'boo')

    def test_url_setting_and_environment(self):
        """
        Settings are loaded from external settings and local environment "wins"

        """
        url_map = {'https://example.invalid/s1': {'FOO': 'bar', 'BING': 'boo'}}
        with self.url_map(url_map), self.environment(XXX_FOO='buzz'):
            load_external_settings(
                settings.__dict__, urls=['https://example.invalid/s1'],
                optional_settings=['FOO', 'BING'], env_prefix='XXX_')
            self.assertEqual(settings.FOO, 'buzz')
            self.assertEqual(settings.BING, 'boo')

    def test_optional_settings(self):
        """
        Specifying a setting as optional allows it to be loaded.

        """
        url_map = {'https://example.invalid/s1': {'FOO': 'bar', 'BING': 'boo'}}

        # Trying to set BING fails if it is not whitelisted.
        with self.url_map(url_map), self.assertRaises(InvalidExternalSettingsError):
            load_external_settings(
                settings.__dict__, urls=['https://example.invalid/s1'], optional_settings=['FOO'])

        # Trying to set BING succeeds if an optional setting.
        with self.url_map(url_map):
            load_external_settings(
                settings.__dict__, urls=['https://example.invalid/s1'],
                optional_settings=['FOO', 'BING'])
            self.assertEqual(settings.FOO, 'bar')
            self.assertEqual(settings.BING, 'boo')

    def test_required_settings(self):
        """
        Specifying a setting as required allows it to be loaded.

        """
        url_map = {'https://example.invalid/s1': {'FOO': 'bar', 'BING': 'boo'}}

        # Trying to set BING fails if it is not whitelisted.
        with self.url_map(url_map), self.assertRaises(InvalidExternalSettingsError):
            load_external_settings(
                settings.__dict__, urls=['https://example.invalid/s1'], required_settings=['FOO'])

        # Trying to set BING succeeds if an required setting.
        with self.url_map(url_map):
            load_external_settings(
                settings.__dict__, urls=['https://example.invalid/s1'],
                required_settings=['FOO', 'BING'])
            self.assertEqual(settings.FOO, 'bar')
            self.assertEqual(settings.BING, 'boo')

        # Trying to load with missing setting fails.
        with self.url_map(url_map), self.assertRaises(InvalidExternalSettingsError):
            load_external_settings(
                settings.__dict__, urls=['https://example.invalid/s1'],
                required_settings=['FOO', 'BING', 'SOME_OTHER_SETTING'])

    def test_allow_unrecognised_settings(self):
        """
        If allow_unrecognised_settings is set, settings not explicitly in the list are ignored.
        """
        url_map = {'https://example.invalid/s1': {'FOO': 'bar', 'BING': 'boo'}}

        # Trying to set BING fails if it is not whitelisted or allowed.
        with self.url_map(url_map), self.assertRaises(InvalidExternalSettingsError):
            load_external_settings(
                settings.__dict__, urls=['https://example.invalid/s1'], required_settings=['FOO'])

        # BING is ignored if unrecognised settings are allowed
        with self.url_map(url_map):
            load_external_settings(
                settings.__dict__, urls=['https://example.invalid/s1'],
                required_settings=['FOO'], allow_unrecognised_settings=True)
            self.assertEqual(settings.FOO, 'bar')
            self.assertFalse(hasattr(settings, 'BING'))

        # BING is ignored, but left in settings object if unrecognised settings are allowed and
        # strip additional flag is false
        with self.url_map(url_map):
            load_external_settings(
                settings.__dict__, urls=['https://example.invalid/s1'],
                required_settings=['FOO'], allow_unrecognised_settings=True,
                strip_unrecognised_settings=False)
            self.assertEqual(settings.FOO, 'bar')
            self.assertTrue(hasattr(settings, 'BING'))

    def test_merging_settings(self):
        """
        Settings with dict values are deep-merged.

        """
        url_map = {
            'https://example.invalid/s1': {
                'FOO': {'a': 'X', 'b': 'Y', 'c': 'Z'},
            },
            'https://example.invalid/s2': {
                'FOO': {'b': 'B', 'd': '1'},
                'BAR': {'x': 1},
            },
        }
        env = {'XXX_FOO': "{'g': 456, 'f': 123}"}
        existing_settings = {'FOO': {'e': 'E', 'f': 'F'}}

        with self.url_map(url_map), self.settings(**existing_settings), self.environment(**env):
            # Settings have expect value before load.
            self.assertDictEqual(settings.FOO, existing_settings['FOO'])
            self.assertFalse(hasattr(settings, 'BAR'))

            load_external_settings(settings.__dict__, urls=[
                'https://example.invalid/s1', 'https://example.invalid/s2',
            ], optional_settings=['FOO', 'BAR'], env_prefix='XXX_')

            # Settings have been merged as expected.
            self.assertDictEqual(settings.FOO, {
                'a': 'X',
                'b': 'B',
                'c': 'Z',
                'd': '1',
                'e': 'E',
                'f': 123,
                'g': 456,
            })
            self.assertDictEqual(settings.BAR, {'x': 1})

    def test_merge_non_dict(self):
        """
        Merging a non-dict existing setting replaces it.

        """
        url_map = {'https://example.invalid/s1': {'FOO': {'a': 'X'}}}
        with self.url_map(url_map), self.settings(FOO='non-dict'):
            # Setting exists before loading
            self.assertEqual(settings.FOO, 'non-dict')
            load_external_settings(
                settings.__dict__, urls=['https://example.invalid/s1'],
                optional_settings=['FOO'])
            # Setting is changed after
            self.assertDictEqual(settings.FOO, {'a': 'X'})

    def environment(self, **environment):
        """
        Context manager which mocks return value from os.environ to the passed keyword args.

        """
        return unittest.mock.patch.dict('os.environ', environment)

    def url_map(self, url_map):
        """
        Take a dict mapping URLs to Python dicts. Returns a context manager which will mock
        geddit.geddit to return YAML-formatted documents for each URL.

        """
        doc_map = {url: yaml.safe_dump(d) for url, d in url_map.items()}
        return unittest.mock.patch('geddit.geddit', lambda url: doc_map[url])
